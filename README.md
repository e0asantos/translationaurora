General archtitecture
![picture](diagram.png)

# Getting started with angular4 Boilerplate
The next steps are required to make this project up and running
- Install the latest .net core runtime (The runtime used here was 1.1.0 and sdk 1.0.4)
- https://go.microsoft.com/fwlink/?linkid=848823
- make sure the "dotnet" command runs smoothly on your computer
- make sure you have yarn installed
- make sure you have webpack installed
- make sure you have npm installed
- make sure you have nodejs installed

## How to run the project

This project is run in 3 steps:
* compile webpack vendor: webpack --config webpack.config.vendor.js
* compile webpack user typescript: webpack (yes the webpack command only)
* dotnet run



The manifest tell bluemix what is the name of the application and what is the container, change this value


## 1. NPMRC for downloading components from repository

All the components under the registry @cemex must be downloaded with the next .npmrc content


  ```
@cemex:registry=https://cemexpmo.jfrog.io/cemexpmo/api/npm/cmx-ngx-framework/
//cemexpmo.jfrog.io/cemexpmo/api/npm/cmx-ngx-framework/:_password=QVA4Rk1uRDhVckxhSEJkS3NjUGprSzkyZVpt
//cemexpmo.jfrog.io/cemexpmo/api/npm/cmx-ngx-framework/:username=read-only-user
//cemexpmo.jfrog.io/cemexpmo/api/npm/cmx-ngx-framework/:always-auth=true
  ```

That file must be placed in the root of the folder so that " yarn install " or "npm install" can have read access

﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace angularX.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ApiEnvironments",
                columns: table => new
                {
                    ApiEnvironmentId = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Environment = table.Column<string>(nullable: true),
                    EnvironmentISO = table.Column<string>(nullable: true),
                    Region = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApiEnvironments", x => x.ApiEnvironmentId);
                });

            migrationBuilder.CreateTable(
                name: "ApiURLs",
                columns: table => new
                {
                    ApiURLId = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    URL = table.Column<string>(nullable: true),
                    Version = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApiURLs", x => x.ApiURLId);
                });

            migrationBuilder.CreateTable(
                name: "Applications",
                columns: table => new
                {
                    ApplicationItemId = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ApplicationItemDescription = table.Column<string>(nullable: true),
                    ApplicationItemName = table.Column<string>(nullable: true),
                    ApplicationItemTechnicalName = table.Column<string>(nullable: true),
                    AuthorApplication = table.Column<string>(nullable: true),
                    TechnicalMode = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Applications", x => x.ApplicationItemId);
                });

            migrationBuilder.CreateTable(
                name: "Languages",
                columns: table => new
                {
                    LanguageId = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    AuthorLanguage = table.Column<string>(nullable: true),
                    CountryCode = table.Column<string>(nullable: true),
                    CurrencyFormat = table.Column<string>(nullable: true),
                    CurrencyName = table.Column<string>(nullable: true),
                    CurrencySymbol = table.Column<string>(nullable: true),
                    CurrencySymbolFloat = table.Column<string>(nullable: true),
                    DayNames = table.Column<string>(nullable: true),
                    DecimalNumbers = table.Column<int>(nullable: false),
                    DecimalSeparator = table.Column<string>(nullable: true),
                    FormatDate = table.Column<string>(nullable: true),
                    FormatTime = table.Column<string>(nullable: true),
                    LanguageCountry = table.Column<string>(nullable: true),
                    LanguageISO = table.Column<string>(nullable: true),
                    LanguageName = table.Column<string>(nullable: true),
                    MonthNames = table.Column<string>(nullable: true),
                    ThousandSeparator = table.Column<string>(nullable: true),
                    textFloat = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Languages", x => x.LanguageId);
                });

            migrationBuilder.CreateTable(
                name: "TranslationKeys",
                columns: table => new
                {
                    TranslationKeyId = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    AuthorKey = table.Column<string>(nullable: true),
                    Key = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TranslationKeys", x => x.TranslationKeyId);
                });

            migrationBuilder.CreateTable(
                name: "ApiRecords",
                columns: table => new
                {
                    ApiRecordId = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ApiEnvironmentId = table.Column<int>(nullable: true),
                    ApiKey = table.Column<string>(nullable: true),
                    ApiURLId = table.Column<int>(nullable: true),
                    AppContainer = table.Column<string>(nullable: true),
                    AppIdApplicationItemId = table.Column<int>(nullable: true),
                    AuthorEmail = table.Column<string>(nullable: true),
                    DateCreated = table.Column<int>(nullable: false),
                    Disabled = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApiRecords", x => x.ApiRecordId);
                    table.ForeignKey(
                        name: "FK_ApiRecords_ApiEnvironments_ApiEnvironmentId",
                        column: x => x.ApiEnvironmentId,
                        principalTable: "ApiEnvironments",
                        principalColumn: "ApiEnvironmentId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ApiRecords_ApiURLs_ApiURLId",
                        column: x => x.ApiURLId,
                        principalTable: "ApiURLs",
                        principalColumn: "ApiURLId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ApiRecords_Applications_AppIdApplicationItemId",
                        column: x => x.AppIdApplicationItemId,
                        principalTable: "Applications",
                        principalColumn: "ApplicationItemId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TranslationValues",
                columns: table => new
                {
                    TranslationValueId = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ApplicationIdApplicationItemId = table.Column<int>(nullable: true),
                    AuthorValue = table.Column<string>(nullable: true),
                    LanguageISOLanguageId = table.Column<int>(nullable: true),
                    Translation = table.Column<string>(nullable: true),
                    TranslationKeyReferencedTranslationKeyId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TranslationValues", x => x.TranslationValueId);
                    table.ForeignKey(
                        name: "FK_TranslationValues_Applications_ApplicationIdApplicationItemId",
                        column: x => x.ApplicationIdApplicationItemId,
                        principalTable: "Applications",
                        principalColumn: "ApplicationItemId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TranslationValues_Languages_LanguageISOLanguageId",
                        column: x => x.LanguageISOLanguageId,
                        principalTable: "Languages",
                        principalColumn: "LanguageId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TranslationValues_TranslationKeys_TranslationKeyReferencedTranslationKeyId",
                        column: x => x.TranslationKeyReferencedTranslationKeyId,
                        principalTable: "TranslationKeys",
                        principalColumn: "TranslationKeyId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ApiRecords_ApiEnvironmentId",
                table: "ApiRecords",
                column: "ApiEnvironmentId");

            migrationBuilder.CreateIndex(
                name: "IX_ApiRecords_ApiURLId",
                table: "ApiRecords",
                column: "ApiURLId");

            migrationBuilder.CreateIndex(
                name: "IX_ApiRecords_AppIdApplicationItemId",
                table: "ApiRecords",
                column: "AppIdApplicationItemId");

            migrationBuilder.CreateIndex(
                name: "IX_TranslationValues_ApplicationIdApplicationItemId",
                table: "TranslationValues",
                column: "ApplicationIdApplicationItemId");

            migrationBuilder.CreateIndex(
                name: "IX_TranslationValues_LanguageISOLanguageId",
                table: "TranslationValues",
                column: "LanguageISOLanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_TranslationValues_TranslationKeyReferencedTranslationKeyId",
                table: "TranslationValues",
                column: "TranslationKeyReferencedTranslationKeyId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ApiRecords");

            migrationBuilder.DropTable(
                name: "TranslationValues");

            migrationBuilder.DropTable(
                name: "ApiEnvironments");

            migrationBuilder.DropTable(
                name: "ApiURLs");

            migrationBuilder.DropTable(
                name: "Applications");

            migrationBuilder.DropTable(
                name: "Languages");

            migrationBuilder.DropTable(
                name: "TranslationKeys");
        }
    }
}

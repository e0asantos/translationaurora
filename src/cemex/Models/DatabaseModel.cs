using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.IO;
using System;
using System.Net;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace angularX
{
    public class TranslationContext : DbContext
    {
        public DbSet<ApplicationItem> Applications { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<TranslationKey> TranslationKeys { get; set; }
        public DbSet<TranslationValue> TranslationValues { get; set; }
        public DbSet<ApiRecord> ApiRecords { get; set; }
        public DbSet<ApiEnvironment> ApiEnvironments { get; set; }
        public DbSet<ApiURL> ApiURLs { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (Directory.GetCurrentDirectory().IndexOf("home/vcap")!=-1)
            {
                //is in bluemix cloud
                optionsBuilder.UseMySql(@Environment.GetEnvironmentVariable("CONNECTION_STRING"));
            } else {
                //optionsBuilder.UseSqlite("Data Source=translations.db");
                optionsBuilder.UseMySql(@Environment.GetEnvironmentVariable("CONNECTION_STRING"));
            }
            
        }
    }

    public class ApplicationItem{
        public int ApplicationItemId{get;set;}
        public string ApplicationItemName{get;set;}
        public string ApplicationItemTechnicalName{get;set;}
        public string ApplicationItemDescription{get;set;}
        public string AuthorApplication{get;set;}
        public int TechnicalMode{get;set;}
    }

    public class Language{
        public int LanguageId{get;set;}
        public string LanguageName{get;set;}
        public string LanguageCountry{get;set;}
        public string LanguageISO{get;set;}
        public string AuthorLanguage{get;set;}
        public string CurrencySymbol{get;set;}
        public string CurrencyFormat{get;set;}
        public string CurrencySymbolFloat{get;set;}
        public string CurrencyName{get;set;}
        public string FormatDate{get;set;}
        public string FormatTime{get;set;}
        public string DecimalSeparator{get;set;}
        public int DecimalNumbers{get;set;}
        public string ThousandSeparator{get;set;}
        public string textFloat { get; set; }
        public string MonthNames { get; set; }
        public string DayNames { get; set; }
        public string CountryCode { get; set; }
    }

    public class TranslationKey{
        public int TranslationKeyId{get;set;}
        public string Key{get;set;}
        public string AuthorKey{get;set;}
    }
    public class TranslationValue{
        public int TranslationValueId{get;set;}
        public TranslationKey TranslationKeyReferenced{get;set;}
        public string Translation{get;set;}
        public Language LanguageISO{get;set;}
        public ApplicationItem ApplicationId{get;set;}
        public string AuthorValue{get;set;}
    }

    public class TranslationDTO{
        public string translationKeyString{get;set;}
        public string Translation{get;set;}
        public int LanguageISO{get;set;}
        public int ApplicationId{get;set;}
        
    }

    public class MassUploadDTO{
        public int LanguageId;
        public int ApplicationId;
        public KeyPairTranslation[] MassJson;
    }
    public class KeyPairTranslation{
        public string Key;
        public string Value;
    }
    
    public class ApiRecord {
        public int ApiRecordId{ get; set; }
        public ApplicationItem AppId{get;set;}
        public string Name{get;set;}
        public string AppContainer{get;set;}
        public ApiEnvironment ApiEnv{get;set;}
        public ApiURL ApiURL{get;set;}
        public int DateCreated{get;set;}
        public string ApiKey{get;set;}
        public string AuthorEmail{get;set;}
        public bool Disabled { get; set; }
    }

    public class ApiEnvironment {
        public int ApiEnvironmentId{get;set;}
        public string Environment{get;set;}
        public string EnvironmentISO{get;set;}
        public string Region{get;set;}
    }
    
    public class ApiURL{
        public int ApiURLId{get;set;}
        public string URL{get;set;}
        public string Version{get;set;}
    }


}
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections;
using Microsoft.AspNetCore.Http;
using System.Net.Http;
using System.IO;
using Newtonsoft.Json;

using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;

namespace angularX.Controllers
{
    [Route("api/[controller]")]
    public class TranslationController : Controller
    {
        private TranslationContext db;
        private static readonly HttpClient client = new HttpClient();
        public TranslationController(){
            this.db = new TranslationContext();
        }
        

        // post: api/translation/createApp
        /*
        the ApplicationItem class is stored in the DatabaseModel
         */
        [HttpPost("[action]")]
        public IActionResult createApp([FromBody] ApplicationItem newApplication)
        {
            // if(HttpContext.Session.GetString("isUserLoggedIn")==null){
            //     return NotFound("Your session has ended, please login again");
            // }
            
            var query=this.db.Applications.Where(u => u.ApplicationItemName.ToLower()==newApplication.ApplicationItemName.ToLower());
            if(query.Count()>0){
                //alredy saved with that name
                return NotFound("Already saved an App resource with the same name");
            } else {
                this.db.Applications.Add(new ApplicationItem { ApplicationItemName = newApplication.ApplicationItemName,
                                        ApplicationItemDescription=newApplication.ApplicationItemDescription,
                                        ApplicationItemTechnicalName=newApplication.ApplicationItemTechnicalName });
                this.db.SaveChanges();
                return Json(this.db.Applications);            
            }
        }

        [HttpGet("[action]")]
        public IActionResult getApps()
        {
            return Json(this.db.Applications);            
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> getSentiment(string translation)
        {
             var values = new Dictionary<string, string>
                {
                    
                };
            string jsonResponse=JsonConvert.SerializeObject(values, Formatting.Indented);
            StringContent jsonStringContent=new StringContent(jsonResponse, Encoding.UTF8, "application/json");
            //client.DefaultRequestHeaders.Add("x-api-key","ZWjy5zB6cGa5OKTzIl5JS6LuopNDr51C274lKGBU");
            //client.DefaultRequestHeaders.Add("Content-Type","application/json");
            
            var response = await client.PostAsync("https://y5xljsiw7j.execute-api.us-east-1.amazonaws.com/default/translatePy?text="+translation+"&source=en&destination=es", jsonStringContent);
            var responseString = await response.Content.ReadAsStringAsync();
            //Console.WriteLine($"{pingMessage}\tscheduled: {timeSnapshot.ToString("o")}");
            Console.WriteLine(responseString);
            return Json(JsonConvert.DeserializeObject(responseString));            
        }

         [HttpGet("[action]/{appId}/{langISO}")]
        public IActionResult translate(string appId,string langISO)
        {
            Hashtable genericTranslation = new Hashtable();
            Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            var queryLang=this.db.Languages.Where(lang=>lang.LanguageISO==langISO).FirstOrDefault();
            var underscoreLang=langISO;
            if(queryLang==null && langISO.IndexOf("-")!=-1){
                underscoreLang=langISO.Replace("-","_");
                queryLang=this.db.Languages.Where(lang=>lang.LanguageISO==underscoreLang).FirstOrDefault();
            } else if(queryLang==null && langISO.IndexOf("_")!=-1){
                underscoreLang=langISO.Replace("_","-");
                queryLang=this.db.Languages.Where(lang=>lang.LanguageISO==underscoreLang).FirstOrDefault();
            }
            var queryApp=this.db.Applications.Where(app=>app.ApplicationItemTechnicalName==appId).FirstOrDefault();
            if(queryApp==null || queryLang==null){
                return Json(genericTranslation);
            }
            var query=this.db.TranslationValues.Include(o=>o.TranslationKeyReferenced)
                                                //Include(s=>s.ApplicationId)
                                                .Where(inside=>inside.LanguageISO.LanguageISO==underscoreLang)
                                                .Where(inside=>inside.ApplicationId.ApplicationItemTechnicalName==appId);
            
            genericTranslation.Add("global.currencySymbol",queryLang.CurrencySymbol);
            genericTranslation.Add("global.currencyFormat",queryLang.CurrencyFormat);
            genericTranslation.Add("global.currencyName",queryLang.CurrencyName);
            genericTranslation.Add("global.formatDate",queryLang.FormatDate);
            genericTranslation.Add("global.formatTime",queryLang.FormatTime);
            genericTranslation.Add("global.decimalSeparator",queryLang.DecimalSeparator);
            genericTranslation.Add("global.decimalNumbers",queryLang.DecimalNumbers);
            genericTranslation.Add("global.thousandSeparator",queryLang.ThousandSeparator);
            genericTranslation.Add("global.textFloat",queryLang.textFloat);
            genericTranslation.Add("global.languageName",queryLang.LanguageName);
            genericTranslation.Add("global.country",queryLang.LanguageCountry);
            genericTranslation.Add("global.currencySymbolFloat",queryLang.CurrencySymbolFloat);

            foreach (var item in query)
            {
                if((unixTimestamp-10)<queryApp.TechnicalMode){
                    genericTranslation.Add(item.TranslationKeyReferenced.Key,item.TranslationKeyReferenced.Key);
                } else {
                    if(!genericTranslation.ContainsKey(item.TranslationKeyReferenced.Key)){
                        genericTranslation.Add(item.TranslationKeyReferenced.Key,item.Translation);
                    }
                    
                }
                
            }
            return Json(genericTranslation);
        }

        // Cache version for usage as .json
        
        // [HttpGet("[action]/{appId}/{langVersioned}")]
        // public IActionResult translateJson(string appId,string langVersioned)
        // {
        //     Hashtable genericTranslation = new Hashtable();
        //     Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
        //     var queryLang=this.db.Languages.Where(lang=>lang.LanguageISO==langVersioned).FirstOrDefault();
        //     var queryApp=this.db.Applications.Where(app=>app.ApplicationItemTechnicalName==appId).FirstOrDefault();
        //     if(queryApp==null || queryLang==null){
        //         return Json(genericTranslation);
        //     }
        //     var query=this.db.TranslationValues.Include(o=>o.TranslationKeyReferenced)
        //                                         //Include(s=>s.ApplicationId)
        //                                         .Where(inside=>inside.LanguageISO.LanguageISO==langVersioned)
        //                                         .Where(inside=>inside.ApplicationId.ApplicationItemTechnicalName==appId);
            
        //     genericTranslation.Add("global.currencySymbol",queryLang.CurrencySymbol);
        //     genericTranslation.Add("global.currencyFormat",queryLang.CurrencyFormat);
        //     genericTranslation.Add("global.currencyName",queryLang.CurrencyName);
        //     genericTranslation.Add("global.formatDate",queryLang.FormatDate);
        //     genericTranslation.Add("global.formatTime",queryLang.FormatTime);
        //     genericTranslation.Add("global.decimalSeparator",queryLang.DecimalSeparator);
        //     genericTranslation.Add("global.decimalNumbers",queryLang.DecimalNumbers);
        //     genericTranslation.Add("global.thousandSeparator",queryLang.ThousandSeparator);
        //     genericTranslation.Add("global.textFloat",queryLang.textFloat);
        //     genericTranslation.Add("global.languageName",queryLang.LanguageName);
        //     genericTranslation.Add("global.country",queryLang.LanguageCountry);
        //     genericTranslation.Add("global.currencySymbolFloat",queryLang.CurrencySymbolFloat);

        //     foreach (var item in query)
        //     {
        //         if((unixTimestamp-10)<queryApp.TechnicalMode){
        //             genericTranslation.Add(item.TranslationKeyReferenced.Key,item.TranslationKeyReferenced.Key);
        //         } else {
        //             if(!genericTranslation.ContainsKey(item.TranslationKeyReferenced.Key)){
        //                 genericTranslation.Add(item.TranslationKeyReferenced.Key,item.Translation);
        //             }
                    
        //         }
                
        //     }
        //     return Json(genericTranslation);
        // }

        [HttpDelete("[action]")]
        public IActionResult removeApp([FromBody] ApplicationItem newApplication)
        {
            // if(HttpContext.Session.GetString("isUserLoggedIn")==null){
            //     return NotFound("Your session has ended, please login again");
            // }
            this.db.Applications.Add(new ApplicationItem { ApplicationItemName = newApplication.ApplicationItemName,ApplicationItemDescription=newApplication.ApplicationItemDescription });
            this.db.SaveChanges();
            return Json(this.db.Applications);
        }
        [HttpPatch("[action]")]
        public IActionResult editApp([FromBody] ApplicationItem newApplication)
        {
            // if(HttpContext.Session.GetString("isUserLoggedIn")==null){
            //     return NotFound("Your session has ended, please login again");
            // }
            this.db.Applications.Add(new ApplicationItem { ApplicationItemName = newApplication.ApplicationItemName,ApplicationItemDescription=newApplication.ApplicationItemDescription });
            this.db.SaveChanges();
            return Json(this.db.Applications);
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> createTranslation([FromBody] TranslationDTO newTranslation)
        {
            // if(HttpContext.Session.GetString("isUserLoggedIn")==null){
            //     return NotFound("Your session has ended, please login again");
            // }
            var query=this.db.TranslationKeys.Where(u => u.Key==newTranslation.translationKeyString).LastOrDefault();;
            if(query==null){
                //create the key
                this.db.TranslationKeys.Add(new TranslationKey{Key=newTranslation.translationKeyString});
                this.db.SaveChanges();
                query=this.db.TranslationKeys.Where(u => u.Key==newTranslation.translationKeyString).LastOrDefault();;
            }
            //now verify if the key exists for the application
             var verify=this.db.TranslationValues.Where(singleValue=>singleValue.ApplicationId.ApplicationItemId==newTranslation.ApplicationId)
                                                 .Where(singleValue=>singleValue.LanguageISO.LanguageId==newTranslation.LanguageISO)
                                                 .Where(singleValue=>singleValue.TranslationKeyReferenced.TranslationKeyId==query.TranslationKeyId);
            if(verify.Count()>0){
                return NotFound("Already saved a translation resource with the same key");
            } else {
                var latestTranslation=this.db.TranslationValues.Add(new TranslationValue{ApplicationId=this.db.Applications.Find(newTranslation.ApplicationId),
                                                                    LanguageISO=this.db.Languages.Find(newTranslation.LanguageISO),
                                                                    Translation=newTranslation.Translation,
                                                                    TranslationKeyReferenced=this.db.TranslationKeys.Find(query.TranslationKeyId)});
                this.db.SaveChanges();
                Console.WriteLine(":::::::::");
                Console.WriteLine(newTranslation.ApplicationId);
                Console.WriteLine(newTranslation.LanguageISO);
                Console.WriteLine(query.TranslationKeyId);
                Console.WriteLine(":::::::::");
                var values = new Dictionary<string, string>
                {
                    
                };
                string jsonResponse=JsonConvert.SerializeObject(values, Formatting.Indented);
                StringContent jsonStringContent=new StringContent(jsonResponse, Encoding.UTF8, "application/json");
                //client.DefaultRequestHeaders.Add("x-api-key","ZWjy5zB6cGa5OKTzIl5JS6LuopNDr51C274lKGBU");
                //client.DefaultRequestHeaders.Add("Content-Type","application/json");
                
                var response = await client.PostAsync("https://p01pxksawa.execute-api.us-east-1.amazonaws.com/default/replicateTranslationPy?text=texto&source=en&destination=es&appId="+newTranslation.ApplicationId+"&langId="+newTranslation.LanguageISO+"&keyId="+query.TranslationKeyId, jsonStringContent);
                var responseString = await response.Content.ReadAsStringAsync();
                //Console.WriteLine($"{pingMessage}\tscheduled: {timeSnapshot.ToString("o")}");
                Console.WriteLine(responseString);
                return Json(this.db.TranslationValues.LastOrDefault());
            }
        }

        [HttpPost("[action]")]
        public IActionResult massTranslation([FromBody] MassUploadDTO massUpload)
        {
            // if(HttpContext.Session.GetString("isUserLoggedIn")==null){
            //     return NotFound("Your session has ended, please login again");
            // }
            //verify if there are new keys
            for (int i = 0; i < massUpload.MassJson.Length; i++)
            {
                var query=this.db.TranslationKeys.Where(u => u.Key == massUpload.MassJson[i].Key).LastOrDefault();
                 if(query==null){
                    //create the key
                    this.db.TranslationKeys.Add(new TranslationKey{Key=massUpload.MassJson[i].Key});
                    this.db.SaveChanges();
                    query=this.db.TranslationKeys.Where(u => u.Key == massUpload.MassJson[i].Key).LastOrDefault();
                }
                //verify if the key exists, if it does, skip the translation
                var verify=this.db.TranslationValues
                            .Where(singleValue=>singleValue.ApplicationId.ApplicationItemId == massUpload.ApplicationId)
                            .Where(singleValue=>singleValue.LanguageISO.LanguageId == massUpload.LanguageId)
                            .Where(singleValue=>singleValue.TranslationKeyReferenced.TranslationKeyId == query.TranslationKeyId);
                if(verify.Count()==0){
                    var latestTranslation=this.db.TranslationValues.Add(new TranslationValue {
                        ApplicationId =this.db.Applications.Find(massUpload.ApplicationId),
                        LanguageISO=this.db.Languages.Find(massUpload.LanguageId),
                        Translation=massUpload.MassJson[i].Value,
                        TranslationKeyReferenced=this.db.TranslationKeys.Find(query.TranslationKeyId)});
                    this.db.SaveChanges();
                } else {
                    TranslationKey findTKeyObject = this.db.TranslationKeys.Where(u => u.Key == massUpload.MassJson[i].Key).LastOrDefault();
                    TranslationValue findTValueObject = this.db.TranslationValues.Where(u => u.TranslationKeyReferenced.TranslationKeyId == findTKeyObject.TranslationKeyId 
                                                                                        && u.LanguageISO.LanguageId == massUpload.LanguageId
                                                                                        && u.ApplicationId.ApplicationItemId == massUpload.ApplicationId).LastOrDefault();
                    findTValueObject.Translation = massUpload.MassJson[i].Value;
                    this.db.SaveChanges();
                }
            }
            return Json(this.db.Applications.Find(massUpload.ApplicationId));
        }

        [HttpGet("[action]")]
        public IActionResult getTranslations()
        {
            var query=this.db.TranslationValues.FromSql("select * from TranslationValues inner join TranslationKeys on TranslationValues.TranslationKeyReferencedTranslationKeyId=TranslationKeys.TranslationKeyId");
            return Json(query);
        }

        [HttpGet("[action]/{appId}/{langId}")]
        public IActionResult getTranslationsByFilter([FromRoute] int appId,[FromRoute] int langId)
        {
            var query=this.db.TranslationValues.Include(o=>o.TranslationKeyReferenced)
                                               .Include(s=>s.ApplicationId)
                                               .Include(lang=>lang.LanguageISO)
                                               .Where(t=>t.ApplicationId.ApplicationItemId==appId)
                                               .Where(t=>t.LanguageISO.LanguageId==langId);
            return Json(query);
        }

        [HttpPost("[action]")]
        public IActionResult createLanguage([FromBody] Language newLanguage)
        {
            // if(HttpContext.Session.GetString("isUserLoggedIn")==null){
            //     return NotFound("Your session has ended, please login again");
            // }
            var query=this.db.Languages.Where(u => u.LanguageISO.ToLower()==newLanguage.LanguageISO.ToLower()).FirstOrDefault();
            if(query!=null){
                //alredy saved with that name
                return NotFound("Already saved a language resource with the same ISO");
            } else {
                this.db.Languages.Add(newLanguage);
                this.db.SaveChanges();
                return Json(this.db.Languages);       
            }
            
        }
        [HttpGet("[action]")]
        public IActionResult getLanguages()
        {
            return Json(this.db.Languages);            
        }

        [HttpGet("[action]")]
        public IActionResult getRegionalLanguages()
        {
            return Json(this.db.Languages);            
        }

        [HttpPut("edit/{id}")]
        public IActionResult editTranslation([FromRoute] int id,[FromBody] TranslationDTO updateTranslation)
        {
            // if(HttpContext.Session.GetString("isUserLoggedIn")==null){
            //     return NotFound("Your session has ended, please login again");
            // }
            var comingTranslation=this.db.TranslationValues.Find(id);
            comingTranslation.Translation=updateTranslation.Translation;
            this.db.SaveChanges();
            return Json(comingTranslation);
            
        }

        [HttpPut("technicalMode/{id}")]
        public IActionResult activateTechnicalMode([FromRoute] int id,[FromBody] ApplicationItem appToChange)
        {
            // if(HttpContext.Session.GetString("isUserLoggedIn")==null){
            //     return NotFound("Your session has ended, please login again");
            // }
            var app=this.db.Applications.Find(id);
            app.TechnicalMode=(Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            this.db.SaveChanges();
            return Json(app);
            
        }

        [HttpPut("language/edit/{id}")]
        public IActionResult editLanguage([FromRoute] int id,[FromBody] Language updateLanguage)
        {
            // if(HttpContext.Session.GetString("isUserLoggedIn")==null){
            //     return NotFound("Your session has ended, please login again");
            // }
            var comingTranslation=this.db.Languages.AsNoTracking().Where(lang=>lang.LanguageId==id).FirstOrDefault();
            if(comingTranslation==null){
                //alredy saved with that name
                return NotFound("Language resource not found");
            } else {
                this.db.Languages.Update(updateLanguage);
                this.db.SaveChanges();
            }
            return Json(comingTranslation);   
        }

        [HttpPost("[action]")]
        public IActionResult translateKey([FromBody] ApplicationItem newApplication)
        {
            // if(HttpContext.Session.GetString("isUserLoggedIn")==null){
            //     return NotFound("Your session has ended, please login again");
            // }
            this.db.Applications.Add(new ApplicationItem { ApplicationItemName = newApplication.ApplicationItemName,ApplicationItemDescription=newApplication.ApplicationItemDescription });
            this.db.SaveChanges();
            return Json(this.db.Applications);
        }

        [HttpGet("[action]")]
        public string getMyRoot()
        {
            //return Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            return Directory.GetCurrentDirectory();
        }
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;


namespace angularX.Controllers
{
    [Route("apis/[controller]")]
    public class ApiRecordController : Controller
    {
        private readonly ILogger _logger;
        private TranslationContext db;
        public ApiRecordController(ILogger<ApiRecordController> logger){
            _logger = logger;
            this.db = new TranslationContext();
        }

        [HttpGet("[action]")]
        public IActionResult getApps()
        {
            return Json(this.db.Applications);            
        }

        //new api record
        [HttpPost("[action]")]
        public IActionResult createApiRecord([FromBody] ApiRecord newApiRecord)
        {
            var query=this.db.ApiRecords.Where(u => u.ApiKey.ToLower() == newApiRecord.ApiKey.ToLower()).FirstOrDefault();
            if(query!=null){
                //alredy saved with that name
                return NotFound("Already saved an Api resource with the same Name");
            } else {
                newApiRecord.AppId = this.db.Applications.Where(u => u.ApplicationItemId == Convert.ToInt32(newApiRecord.AppId.ApplicationItemName)).FirstOrDefault();
                newApiRecord.ApiEnv = this.db.ApiEnvironments.Where(u => u.ApiEnvironmentId == newApiRecord.ApiEnv.ApiEnvironmentId).FirstOrDefault();
                newApiRecord.ApiURL = this.db.ApiURLs.Where(u => u.ApiURLId == newApiRecord.ApiURL.ApiURLId).FirstOrDefault();
                this.db.ApiRecords.Add(newApiRecord);
                this.db.SaveChanges();

                return Json(this.db.ApiRecords.LastOrDefault());
            }
        }

        [HttpGet("[action]")]
        public IActionResult getApiRecords()
        {
            var query=this.db.ApiRecords.Include(env=>env.ApiEnv).Include(app=>app.AppId).Include(url=>url.ApiURL);
            return Json(query);
        }

        // URLS
        [HttpPost("[action]")]
        public IActionResult createURL([FromBody] ApiURL newURL)
        {
            var query=this.db.ApiURLs.Where(u => u.URL.ToLower()==newURL.URL.ToLower()).FirstOrDefault();
            
            if(query!=null){
                //alredy saved with that name
                return NotFound("Already saved a url resource with the same ISO" );
            } else {
                newURL.ApiURLId=0;
                this.db.ApiURLs.Add(newURL);
                this.db.SaveChanges();
                return Json(this.db.ApiURLs);       
            }
        }

        [HttpGet("[action]")]
        public IActionResult getURLs()
        {
            return Json(this.db.ApiURLs);            
        }
        // ENVIRONMENT
        [HttpPost("[action]")]
        public IActionResult createEnvironment([FromBody] ApiEnvironment newEnvironment)
        {
            var query=this.db.ApiEnvironments.Where(u => u.EnvironmentISO.ToLower() == newEnvironment.EnvironmentISO.ToLower()).FirstOrDefault();
            if(query!=null){
                //alredy saved with that name
                return NotFound("Already saved an environment resource with the same ISO");
            } else {
                newEnvironment.ApiEnvironmentId=0;
                this.db.ApiEnvironments.Add(newEnvironment);
                this.db.SaveChanges();
                return Json(this.db.ApiEnvironments);       
            }
            
        }

        [HttpGet("[action]")]
        public IActionResult getEnvironments()
        {
            return Json(this.db.ApiEnvironments);            
        }

        [HttpPut("apiRecord/edit/{id}")]
        public IActionResult editApiRecord([FromRoute] int id, [FromBody] ApiRecord updateApiRecord)
        {
            var comingApiRecord = this.db.ApiRecords.Where(u => u.ApiRecordId == id).FirstOrDefault();

            if (comingApiRecord == null){
                return NotFound("Api record does not exist");
            } else {
                // Update the required fields
                var appId = this.db.Applications.Where(u => u.ApplicationItemId == Convert.ToUInt32(updateApiRecord.AppId.ApplicationItemName)).FirstOrDefault();
                var ApiURL = this.db.ApiURLs.Where(u => u.ApiURLId == updateApiRecord.ApiURL.ApiURLId).FirstOrDefault();
                var ApiEnv = this.db.ApiEnvironments.Where(u => u.ApiEnvironmentId == updateApiRecord.ApiEnv.ApiEnvironmentId).FirstOrDefault();

                if (appId != null && ApiURL != null && ApiEnv != null)
                {
                    comingApiRecord.AppId = appId;
                    comingApiRecord.ApiURL = ApiURL;
                    comingApiRecord.ApiEnv = ApiEnv;
                    comingApiRecord.Disabled = updateApiRecord.Disabled;
                    this.db.SaveChanges();

                    return Json(comingApiRecord);
                }
                else
                {
                    return NotFound("Some records are non existing");
                }
            }    
        }

        [HttpPut("technicalMode/{id}")]
        public IActionResult activateTechnicalMode([FromRoute] int id,[FromBody] ApplicationItem appToChange)
        {
            if(HttpContext.Session.GetString("isUserLoggedIn")==null){
                return NotFound("User not logged in");
            }
            var app=this.db.Applications.Find(id);
            app.TechnicalMode=(Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            this.db.SaveChanges();
            return Json(app);
            
        }

        [HttpPut("url/edit/{id}")]
        public IActionResult editURL([FromRoute] int id,[FromBody] ApiURL updateURL)
        {
            if(HttpContext.Session.GetString("isUserLoggedIn")==null){
                return NotFound("User not logged in");
            }
            var comingURL=this.db.ApiURLs.AsNoTracking().Where(url=>url.ApiURLId==id).FirstOrDefault();
            if(comingURL==null){
                //alredy saved with that name
                return NotFound("Language resource not found");
            } else {
                this.db.ApiURLs.Update(updateURL);
                this.db.SaveChanges();
            }
            return Json(comingURL);   
        }

        [HttpPost("[action]")]
        public IActionResult translateKey([FromBody] ApplicationItem newApplication)
        {
            if(HttpContext.Session.GetString("isUserLoggedIn")==null){
                return NotFound("User not logged in");
            }
            this.db.Applications.Add(new ApplicationItem { ApplicationItemName = newApplication.ApplicationItemName,
                                                           ApplicationItemDescription=newApplication.ApplicationItemDescription });
            this.db.SaveChanges();
            return Json(this.db.Applications);
        }

        [HttpGet("[action]")]
        public string getMyRoot()
        {
            //return Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            return Directory.GetCurrentDirectory();
        }
    }
}

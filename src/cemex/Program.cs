using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;


namespace angularX
{
    public class Program
    {
        public static void Main(string[] args)
        {

            // using (var db = new TranslationContext())
            // {
            //     db.Applications.Add(new ApplicationItem { ApplicationItemName = "CustomerInformation",ApplicationItemDescription="Description of the application" });
            //     var count = db.SaveChanges();
            //     Console.WriteLine("{0} records saved to database", count);

            //     Console.WriteLine();
            //     Console.WriteLine("All blogs in database:");
            //     foreach (var applications in db.Applications)
            //     {
            //         Console.WriteLine(" - {0}", applications.ApplicationItemName);
            //     }
            // }

            var host = new WebHostBuilder()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .ConfigureLogging((hostingContext, logging) =>
                {
                    logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                    logging.AddConsole();
                    logging.AddDebug();
                })
                .UseStartup<Startup>()
                .Build();

            host.Run();

            
        }
    }
}

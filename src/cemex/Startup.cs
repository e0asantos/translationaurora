using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Chroniton;
using Chroniton.Jobs;
using Chroniton.Schedules;
using System.Net.Http;
using System.IO;
using Newtonsoft.Json;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;

namespace angularX
{
    public class Startup
    {
        private static readonly HttpClient client = new HttpClient();
        public Startup(IHostingEnvironment env)
        {
            System.Console.WriteLine("starting...");
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();

            var factory = new SingularityFactory();
            var singularity = factory.GetSingularity();

            // var job = new SimpleParameterizedJob<string>((parameter, scheduledTime) => 
            //     pingAliveToServer(parameter,scheduledTime)
            //     );

            // var schedule = new EveryXTimeSchedule(TimeSpan.FromSeconds(60));
            // var scheduledJob = singularity.ScheduleParameterizedJob(
            //     schedule, job, "PING: ", true); //starts immediately
            // singularity.Start();
            
        }
        public async Task pingAliveToServer(string pingMessage,DateTime timeSnapshot){
            var values = new Dictionary<string, string>
                {
                    { "DigitalServiceName", "ngXboilerPlate" },
                    { "TechnicalServiceName", "ngxboilerPlate" }
                };
            string jsonResponse=JsonConvert.SerializeObject(values, Formatting.Indented);
            StringContent jsonStringContent=new StringContent(jsonResponse, Encoding.UTF8, "application/json");
            
            
            var response = await client.PostAsync("http://server-status.mybluemix.net/api/statusServer/ping", jsonStringContent);
            // var responseString = await response.Content.ReadAsStringAsync();
            // Console.WriteLine($"{pingMessage}\tscheduled: {timeSnapshot.ToString("o")}");
            // Console.WriteLine(responseString);
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Enabling CORS glabally (for all Controllers)
            services.AddCors(options => { options.AddPolicy("AllowAll", builder =>
                {
                    builder
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials();
                });
            });

            // Add framework services.
            services.AddMvc();

            services.AddDistributedMemoryCache(); // Adds a default in-memory implementation of IDistributedCache
            services.AddSession(options =>
            {
                // Set a short timeout for easy testing.
                options.IdleTimeout = TimeSpan.FromSeconds(1500);
                options.Cookie.HttpOnly = true;
            });
            services.AddDbContext<TranslationContext>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions {
                    HotModuleReplacement = true
                });
            }
            else
            {
                app.UseDeveloperExceptionPage();
                //app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseSession();
            app.UseCors("AllowAll");
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
                
                // routes.MapRoute(
                //     "DeepLink",
                //     "{*pathInfo}",
                //     defaults: new { controller = "Proxy", action = "please" });

                routes.MapSpaFallbackRoute(
                    name: "spa-fallback",
                    defaults: new { controller = "Home", action = "Index" });

                
            });
        }
    }
}

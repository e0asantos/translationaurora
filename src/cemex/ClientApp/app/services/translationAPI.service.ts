import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { AlertService } from '@cemex/cmx-alert-v2/dist';

import { ApplicationDTO } from './../types/applicationDTO';
import { LanguageDTO } from '../types/languateDTO';
import { TranslationDTO } from '../types/translationDTO';
import { MassUploadDTO } from '../types/massUploadDTO';

@Injectable()
export class ApplicationService {
    url = "";
    constructor(private http: Http,
                public alert:AlertService) { }
    getApps(): Observable<ApplicationDTO[]> {
        return this.http.get("/api/Translation/getApps")
            .map(this.extractData)
            .catch(this.handleErrorObservable);
    }
    addApplication(app: ApplicationDTO): Observable<ApplicationDTO[]> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post("/api/Translation/createApp", app, options)
            .map(this.extractData)
            .catch(this.handleErrorObservable);
    }
    getLanguages():Observable<LanguageDTO[]>{
        return this.http.get("/api/Translation/getLanguages")
        .map(this.extractData)
        .catch(this.handleErrorObservable);
    }
    addLanguage(lang: LanguageDTO): Observable<LanguageDTO[]> {
        if(lang.languageId==null || lang.languageId==undefined){
            lang.languageId=0;
            let headers = new Headers({ 'Content-Type': 'application/json' });
            let options = new RequestOptions({ headers: headers });
            return this.http.post("/api/Translation/createLanguage", lang, options)
                .map(this.extractData)
                .catch(this.handleErrorObservable);
        } else {
            let headers = new Headers({ 'Content-Type': 'application/json' });
            let options = new RequestOptions({ headers: headers });
            return this.http.put("/api/Translation/language/edit/"+lang.languageId, lang, options)
                .map(this.extractData)
                .catch(this.handleErrorObservable);
        }
        
    }
    getSentiment(label:string): Observable<any> {
        let headers = new Headers({ 'Content-Type': 'application/json','x-api-key':'ZWjy5zB6cGa5OKTzIl5JS6LuopNDr51C274lKGBU' });
        let options = new RequestOptions({ headers: headers });
        return this.http.get("/api/Translation/getSentiment?translation="+label,options)
        .map(this.extractData)
        .catch(this.handleErrorObservable);
    }
    addTranslation(translation: TranslationDTO): Observable<TranslationDTO> {
        console.log("entra a addtranslation con esto: ", translation)
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post("/api/Translation/createTranslation", translation, options)
            .map(this.extractData)
            .catch(this.handleErrorObservable);
    }
    getTranslations():Observable<TranslationDTO[]>{
        return this.http.get("/api/Translation/getTranslations")
        .map(this.extractData)
        .catch(this.handleErrorObservable);
    }
    getTranslationsByFilter(appId:number,languageId:number):Observable<TranslationDTO[]>{
        return this.http.get("/api/Translation/getTranslationsByFilter/"+appId+"/"+languageId)
        .map(this.extractData)
        .catch(this.handleErrorObservable);
    }
    putTranslation(translation: TranslationDTO): Observable<TranslationDTO> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.put("/api/Translation/edit/"+translation.translationValueId, translation, options)
            .map(this.extractData)
            .catch(this.handleErrorObservable);
    }
    putTechnicalMode(app: ApplicationDTO): Observable<TranslationDTO> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.put("/api/Translation/technicalMode/"+app.applicationItemId, app, options)
            .map(this.extractData)
            .catch(this.handleErrorObservable);
    }
    addMassTranslation(translation: MassUploadDTO): Observable<any> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post("/api/Translation/massTranslation", translation, options)
            .map(this.extractData)
            .catch(this.handleErrorObservable);
    }
    activateTechnicalMode():Observable<TranslationDTO[]>{
        return this.http.get("/api/Translation/getTranslations")
        .map(this.extractData)
        .catch(this.handleErrorObservable);
    }
    getBooksWithPromise(): Promise<ApplicationDTO[]> {
        return this.http.get(this.url).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);
    }
    addBookWithPromise(book: ApplicationDTO): Promise<ApplicationDTO> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(this.url, book, options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);
    }
    private extractData(res: Response) {
        let body = res.json();
        return body || {};
    }
    private handleErrorObservable(error: Response | any) {
        //alert("The 15 minutes session expired, please logout and login again");
        return Observable.throw(error.message || error);
    }
    private handleErrorPromise(error: Response | any) {
        alert(error.message || error);
        return Promise.reject(error.message || error);
    }
} 
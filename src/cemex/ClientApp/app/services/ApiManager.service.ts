import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { AlertService } from '@cemex/cmx-alert-v2/dist';
import { ApiRecordDTO } from './../types/apiDTO';
import { ApiEnvironmentDTO } from './../types/apiDTO';
import { ApiURLDTO, ApiApplicationItemDTO } from './../types/apiDTO';

@Injectable()
export class ApiManagerService {
    url = "apis/ApiRecord";
    constructor(private http: Http,
                public alert:AlertService) { }
    
    // APIRecord
    getApiRecords(): Observable<ApiRecordDTO[]> {
        return this.http.get("/apis/ApiRecord/getApiRecords")
            .map(this.extractData)
            .catch(this.handleErrorObservable);
    }
    
    addApiRecord(api: ApiRecordDTO): Observable<ApiRecordDTO[]> {       
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post("/apis/ApiRecord/createApiRecord", api, options)
            .map(this.extractData)
            .catch(this.handleErrorObservable);
    }
    
    // ENVIRONMENT
    getEnvironments():Observable<ApiEnvironmentDTO[]>{
        return this.http.get("/apis/ApiRecord/getEnvironments")
        .map(this.extractData)
        .catch(this.handleErrorObservable);
    }

    addEnvironment(env: ApiEnvironmentDTO): Observable<ApiEnvironmentDTO[]> {
        if (env.apiEnvironmentId == null || env.apiEnvironmentId == undefined) {
            env.apiEnvironmentId=0;
            let headers = new Headers({ 'Content-Type': 'application/json' });
            let options = new RequestOptions({ headers: headers });
            return this.http.post("/apis/ApiRecord/createEnvironment", env, options)
                .map(this.extractData)
                .catch(this.handleErrorObservable);
        } else {
            let headers = new Headers({ 'Content-Type': 'application/json' });
            let options = new RequestOptions({ headers: headers });
            return this.http.put("/apis/ApiRecord/env/edit/"+env.apiEnvironmentId, env, options)
                .map(this.extractData)
                .catch(this.handleErrorObservable);
        }
    }

    putApiRecord(apiRecord: ApiRecordDTO): Observable<ApiRecordDTO> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.put("/apis/ApiRecord/apiRecord/edit/"+apiRecord.apiRecordId, apiRecord, options)
            .map(this.extractData)
            .catch(this.handleErrorObservable);
    }
    
    // URLs
    getURLs():Observable<ApiURLDTO[]>{
        return this.http.get("/apis/ApiRecord/getUrls")
        .map(this.extractData)
        .catch(this.handleErrorObservable);
    }

    addURL(url: ApiURLDTO): Observable<ApiURLDTO[]> {
        if(url.apiURLId==null || url.apiURLId==undefined){
            url.apiURLId=0;
            let headers = new Headers({ 'Content-Type': 'application/json' });
            let options = new RequestOptions({ headers: headers });
            return this.http.post("/apis/ApiRecord/createUrl", url, options)
                .map(this.extractData)
                .catch(this.handleErrorObservable);
        } else {
            let headers = new Headers({ 'Content-Type': 'application/json' });
            let options = new RequestOptions({ headers: headers });
            return this.http.put("/apis/ApiRecord/url/edit/"+url.apiURLId, url, options)
                .map(this.extractData)
                .catch(this.handleErrorObservable);
        }
    }

    private extractData(res: Response) {
        let body = res.json();
        return body || {};
    }
    private handleErrorObservable(error: Response | any) {
        
        return Observable.throw(error.message || error);
    }
    private handleErrorPromise(error: Response | any) {
        
        return Promise.reject(error.message || error);
    }
} 
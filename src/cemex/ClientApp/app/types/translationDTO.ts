import {ApplicationDTO} from './applicationDTO'
export class TranslationDTO{
    constructor(key?:string,
        translation?:string,
        iso?:number,
        appId?:number){
            this.applicationId=appId;
            this.translationKeyString=key;
            this.languageISO=iso
            this.translation=translation;
    }
    translationValueId:number;
    translationKeyReferenced:TranslationKey;
    translationKeyString:string;
    applicationId:any;
    languageISO:any;
    translation:string;
    author:string;

    
    get proxyTranslationKey():string{
        return this.translationKeyReferenced.key
    }
    set proxyTranslationKey(value:string){

    }

    get proxyLanguageName():string{
        return this.languageISO.languageName
    }
    set proxyLanguageName(value:string){

    }

    get proxyLanguageISO():string{
        return this.languageISO.languageISO;
    }
    set proxyLanguageISO(value:string){

    }

    get proxyLanguageCountry():string{
        return this.languageISO.languageCountry;
    }
    set proxyLanguageCountry(value:string){

    }

    get proxyApplicationName():string{
        return this.applicationId.applicationItemName;
    }
    set proxyApplicationName(value:string){

    }
}
export class TranslationKey{
    translationKeyId:number;
    key:string;
    authorKey:string;
}

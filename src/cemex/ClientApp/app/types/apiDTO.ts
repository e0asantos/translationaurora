import { ApplicationDTO } from "./applicationDTO"

export class ApiRecordDTO {
    apiRecordId: number;
    name: string;
    appContainer: string;
    apiEnv: any;
    apiURL: any;
    dateCreated: number;
    apiKey: string;
    authorEmail: string;
    appId: any;
    disabled: boolean = false;

    constructor(
        apiRecordId?: number, 
        name?: string,
        appContainer?: string,
        apiEnv?: any,
        apiURL?: any,        
        dateCreated?: number,       
        apiKey?: string,        
        authorEmail?: string,
        appId?: any,
        disabled?: boolean) {
            this.apiRecordId = apiRecordId;
            this.name = name;
            this.appContainer = appContainer;
            this.apiEnv = apiEnv;
            this.apiURL = apiURL;
            this.dateCreated = dateCreated;
            this.apiKey = apiKey;
            this.authorEmail = authorEmail;
            this.appId = appId;
            this.disabled = disabled;
    }

    get proxyURL():string{
        return this.apiURL.url;
    }

    set proxyURL(value:string){}

    get proxyEnvironment():string{
        return this.apiEnv.environment;
    }

    set proxyEnvironment(value:string){}

    get proxyApplicationName():string{
        return this.appId.applicationItemName;
    }

    set proxyApplicationName(value:string){}
}

export class ApiEnvironmentDTO {
    apiEnvironmentId: number;
    environment: string;
    environmentISO: string;
    region: string;

    constructor(apiEnvironmentId?: number, environment?: string, environmentISO?: string, region?: string) {
        this.apiEnvironmentId = apiEnvironmentId;
        this.environment = environment;
        this.environmentISO = environmentISO;
        this.region = region;
    }
}

export class ApiURLDTO {
    apiURLId: number;
    url: string;
    version: string;

    constructor(apiURLId?: number, url?: string, version?: string) {
        this.apiURLId = apiURLId;
        this.url = url;
        this.version = version;
    }
}

export class ApiApplicationItemDTO {
    aplicationItemId: number;
    applicationItemName: string;
    applicationItemTechnicalName: string;
    applicationItemDescription: string;
    authorApplication: string;
    technicalMode: number;

    constructor(
        aplicationItemId?: number,
        applicationItemName?: string,
        applicationItemTechnicalName?: string,
        applicationItemDescription?: string,
        authorApplication?: string,
        technicalMode?: number,
    ) {
        this.aplicationItemId = aplicationItemId;
        this.applicationItemName = applicationItemName;
        this.applicationItemTechnicalName = applicationItemTechnicalName;
        this.applicationItemDescription = applicationItemDescription;
        this.authorApplication = authorApplication;
        this.technicalMode = technicalMode;
    }
}
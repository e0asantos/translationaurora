export class MassUploadDTO{
    languageId:number;
    applicationId:number;
    massJson:KeyPairTranslation[];
}
export class KeyPairTranslation{
    key:string;
    value:string;
}
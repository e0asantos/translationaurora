import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { sharedConfig } from './app.module.shared';

import {ApplicationService} from './services/translationAPI.service'
import {ApiManagerService} from './services/ApiManager.service'

@NgModule({
    bootstrap: sharedConfig.bootstrap,
    declarations: sharedConfig.declarations,
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        ...sharedConfig.imports
    ],
    providers: [
        { provide: 'ORIGIN_URL', useValue: location.origin },
        { provide: 'USE_TRANSLATION_SERVER', useValue: true },
        { provide: 'PRODUCT_PATH', useValue:'/translation-service/'},
        ApplicationService,
        ApiManagerService
    ]
})
export class AppModule {
}

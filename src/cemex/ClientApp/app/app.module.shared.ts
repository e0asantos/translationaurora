import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpModule } from '@angular/http';

import { AppComponent } from './views/app/app.component';
import { StartComponent } from './views/app/start.view';
import { TestComponent } from './views/test-view/test.view';
import { SetupComponent } from './views/setup-view/setup.view';
import { LoginComponent } from './views/login/login.view';
import { ApiComponent } from './views/api/api.view';
import { ApiConfigComponent } from './views/api-config/api-config.view';

import { CmxCoreCommonModule } from '@cemex-core/angular-services-v2/dist';
import { CmxLoginModule } from '@cemex/cmx-login-v1/dist/index';
import { CmxNavHeaderModule } from '@cemex/cmx-nav-header-v1/dist/index';
import { CmxSidebarModule } from '@cemex/cmx-sidebar-v1/dist';
import { AuthGuard } from '@cemex-core/angular-services-v2/dist';
import { CmxButtonModule } from '@cemex/cmx-button-v1/dist';
import { InputModule } from '@cemex/cmx-input-v3/dist';
import { CmxDropdownModule } from '@cemex/cmx-dropdown-v1/dist'
import { CmxCheckboxModule } from '@cemex/cmx-checkbox-v1/dist';
import { CmxTableModule } from '@cemex/cmx-table-v1/dist';
import { CmxDialogv3Module } from '@cemex/cmx-dialog-v3/dist';
import {CmxPanelCardModule} from '@cemex/cmx-panel-card-v1/dist';
import { AlertModule } from '@cemex/cmx-alert-v2/dist';

import {HttpClientModule} from '@angular/common/http';
import { CmxFlugeeService } from '@cemex/cmx-flugee-v1';

export const sharedConfig: NgModule = {
    imports: [
        BrowserModule,
        CommonModule,
        HttpModule,
        FormsModule, ReactiveFormsModule,
        FlexLayoutModule,
        CmxCoreCommonModule,
        CmxLoginModule,
        CmxTableModule,
        CmxDialogv3Module,
        CmxNavHeaderModule,
        CmxSidebarModule,
        CmxButtonModule,
        InputModule,
        AlertModule,
        CmxCheckboxModule,
        CmxDropdownModule,
        HttpClientModule,
        CmxPanelCardModule,
        RouterModule.forRoot([
            { path: "", redirectTo: 'app', pathMatch: 'full' },
            { path: "login", component: LoginComponent, data: { redirectTo: 'app', } },
            {
                path: "app", component: AppComponent,canActivate:[AuthGuard],
                children: [
                    { path: '', redirectTo: 'home', pathMatch: 'full'},
                    { path: 'home', component: TestComponent },
                    { path: 'setup', component: SetupComponent },
                    { path: 'apis', component: ApiComponent },
                    { path: 'api-config', component: ApiConfigComponent }
                    ,
                ]
            },
            { path: '**', redirectTo: 'app' }
        ])],
    declarations: [
        StartComponent,
        AppComponent,
        TestComponent,
        SetupComponent,
        LoginComponent,
        ApiComponent,
        ApiConfigComponent
    ],
    providers: [CmxFlugeeService],
    bootstrap: [StartComponent]
}
export class AppModuleShared {
}

import { Component, ViewChild, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { CmxSidebarComponent, ICustomOption, ICustomSubOption } from '@cemex/cmx-sidebar-v1/dist';
import { SessionService } from '@cemex-core/angular-services-v2/dist';
import { ILegalEntity } from '@cemex-core/types-v2/dist/index.interface';
import { Broadcaster } from '@cemex-core/events-v1/dist';

@Component({
    selector: 'app-component',
    styleUrls:['./app.component.css'],
    templateUrl:'./app.component.html',
})
export class AppComponent implements OnInit {
    public header: string = 'UMD Demo';
    

    @ViewChild(CmxSidebarComponent)
    sidebar: CmxSidebarComponent;

   

    constructor(private sessionService: SessionService,
        private eventBroadcaster: Broadcaster
    ) {

    }

    ngOnInit() {
        this.eventBroadcaster.on<string>(Broadcaster.DCM_APP_LOGOUT)
            .subscribe((response) => {
                console.log("do some stuff");
            });

        sessionStorage.setItem("my_session_storage", "test value" );

    }

    clickMenuButton(event: any) {
        this.sidebar.isCollapsed = !this.sidebar.isCollapsed;

        console.log("clickMenuButton, isCollapsed:", this.sidebar.isCollapsed);
    }

    private collapseEvent(collapsed: boolean) {
        console.log("sidebar is collapsed:", collapsed);
    }

    private customOptionEvent(value:ICustomSubOption){
        console.log("you clicked on:", value);
    }

}

import { Component } from '@angular/core';
import { SessionService } from '@cemex-core/angular-services-v2/dist';
import { Broadcaster } from '@cemex-core/events-v1/dist';


@Component({
    selector: 'login-component',
    templateUrl:'./login.view.html'
})
export class LoginComponent {

    
    constructor(sessionService:SessionService,
                private eventBroadcaster: Broadcaster) {
                    //sessionService.setBeforeLogout(this.myMethod);
        
    }
    ngOnInit() {
        this.eventBroadcaster.on<string>(Broadcaster.DCM_APP_LOGOUT)
            .subscribe((response) => {
                console.log("do some stuff");
                sessionStorage.removeItem("my_session_storage");
            });

        sessionStorage.setItem("my_session_storage", "test value");

    }

}

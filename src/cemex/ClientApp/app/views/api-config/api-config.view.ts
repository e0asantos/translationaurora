import { Component, ViewChild } from '@angular/core';
import { CmxSidebarComponent, ICustomOption, ICustomSubOption } from '@cemex/cmx-sidebar-v1/dist';
import { ILegalEntity } from '@cemex-core/types-v2/dist/index.interface';
import { Broadcaster } from '@cemex-core/events-v1/dist';
import { SessionService,TranslationService } from '@cemex-core/angular-services-v2/dist';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Column, Table } from '@cemex/cmx-table-v1/dist';
import { CmxDialogV3 } from '@cemex/cmx-dialog-v3/dist';
import { AlertService } from '@cemex/cmx-alert-v2/dist';

import { ApplicationService } from './../../services/translationAPI.service'
import { ApiManagerService } from './../../services/ApiManager.service'
import { ApplicationDTO } from '../../types/applicationDTO';
import { ApiRecordDTO, ApiEnvironmentDTO, ApiURLDTO } from '../../types/apiDTO';

@Component({
    templateUrl: './api-config.view.html',
    styleUrls: ["./api-config.view.scss"]
})
export class ApiConfigComponent {
    formUrl: FormGroup;
    formEnv: FormGroup;
    private envsTable = new Table();
    private envsTableData: any[] = [];

    private urlsTable = new Table();
    private urlsTableData: any[] = [];
    private modalInAppCreate: boolean = true;

    @ViewChild(CmxDialogV3) envDialog: CmxDialogV3;
    @ViewChild(CmxDialogV3) urlDialog: CmxDialogV3;

    constructor(private sessionService: SessionService,
        private eventBroadcaster: Broadcaster,

        private fb: FormBuilder,
        private fbLang: FormBuilder,

        private http: HttpClient,
        public a: AlertService,
        public app: ApplicationService,
        public api: ApiManagerService,
        public t:TranslationService
    ) {
        this.populateTables();
        this.setUrlTableColumns();
        this.setEnvironmentTableColumns();
    }

    public setEnvironmentTableColumns() {
        let envTableColumns: Column[] = [
            new Column('ID', 'apiEnvironmentId', true, true, 'apiEnvironmentId', 'apiEnvironmentId'),
            new Column('Environment name', 'environment', true, true, 'environment', 'environment'),
            new Column('Environment ISO', 'environmentISO', true, true, 'environmentISO', 'environmentISO'),
            new Column('Region', 'region', true, true, 'region', 'region')
        ];
        this.envsTable.setColumns(envTableColumns);
    }
    public setUrlTableColumns() {
        let urlTableColumns: Column[] = [
            new Column('ID', 'apiURLId', true, true, 'apiURLId', 'apiURLId'),
            new Column('URL', 'url', true, true, 'url', 'url'),
            new Column('Version', 'version', true, true, 'version', 'version')

        ];
        this.urlsTable.setColumns(urlTableColumns);
        this.setFilters();
    }

    ngOnInit() {
        this.formEnv = this.fb.group({
            envNameInput: ['', Validators.required],
            envISOInput: ['', Validators.required],
            envRegionInput: ['', Validators.required]
        });
        this.formUrl = this.fb.group({
            urlInput: ['', Validators.required],
            versionInput: ['', Validators.required]
        });
    }

    private populateTables(): void {
        this.api.getEnvironments().subscribe(envs => {
            this.envsTableData = [];
            for (let i = 0; i < envs.length; i++) {
                const aux = {
                    $index: i,
                    environment: envs[i].environment,
                    environmentISO: envs[i].environmentISO,
                    region: envs[i].region,
                    apiEnvironmentId: envs[i].apiEnvironmentId
                };
                this.envsTableData.push(aux);
            }
            //this.setApplicationTableColumns();
        }, error => {
        })

        this.api.getURLs().subscribe(urls => {
            this.urlsTableData = [];
            for (let i = 0; i < urls.length; i++) {
                const aux = Object.assign({
                    $index: i,
                    url: urls[i].url,
                    version: urls[i].version,
                }, urls[i]);
                this.urlsTableData.push(aux);
            }
        }, error => {
        })
    }

    private setFilters(): void {
        this.urlsTableData.forEach((data) => {
            this.urlsTable.getColumn('url').addFilter(data);
            this.urlsTable.getColumn('version').addFilter(data);
            this.urlsTable.getColumn('apiURLlId').addFilter(data);
        });
    }

    private clickDropdownItem(): void {
        alert('Option clicked');
    }

    private saveNewUrl() {
        this.api.addURL(new ApiURLDTO(this.formUrl.value.urlInput, 
            this.formUrl.value.versionInput)).subscribe(urlList => {
            this.formUrl.reset({
                'urlInput': '',
                'versionInput': ''
            })
            this.populateTables();
            this.a.openSuccess('URL details saved correctly', 'Created as: ' + urlList[urlList.length - 1].url, 0);
        },
            error => {
                this.a.openError(error._body, "Try a different value", 0);
            });
    }
    private saveNewEnvironment() {
        this.api.addEnvironment(new ApiEnvironmentDTO(
                null,
                this.formEnv.value.envNameInput,
                this.formEnv.value.envISOInput,
                this.formEnv.value.envRegionInput
            )).subscribe(envList => {
                this.formEnv.reset({
                    'envNameInput': '',
                    'envISOInput': '',
                    'envRegionInput': ''
                })
                this.populateTables();
                this.a.openSuccess('Environment details saved correctly', 'Created as: ' + envList[envList.length - 1].environment + " (" + envList[envList.length - 1].environmentISO + ")", 0);
            },
                error => {
                    
                    this.a.openError(error._body, "Try a different value", 0);
                });
    }


    private editEnvironment(value: ApiEnvironmentDTO) {
        this.modalInAppCreate = false;
        this.envDialog.open();
        this.formEnv.reset({
            'envNameInput': value.environment,
            'envISOInput': value.environmentISO,
            'envRegionInput': value.region
        })
    }
    
    private editUrl(value: ApiURLDTO) {
        this.modalInAppCreate = false;
        this.urlDialog.open();
        this.formEnv.reset({
            'urlInput': value.url,
            'versionInput': value.version
        })
    }

    private openEnvironmentDialog() {
        this.modalInAppCreate = true;
        this.envDialog.open();
    }
    private openURLDialog() {
        this.modalInAppCreate = false;
        this.formEnv.reset({
            'urlInput': '',
            'versionInput': ''
        })
        this.urlDialog.open();
    }

    // private enableTechnicalMode(value){
    //     this.api.putTechnicalMode(value).subscribe(success=>{
    //         alert("Technical mode enabled in selected application for 10 seconds, the technical mode will deactivate by itself.");
    //     })
    // }


}
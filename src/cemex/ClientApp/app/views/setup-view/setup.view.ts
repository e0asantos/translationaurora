import { Component, ViewChild } from '@angular/core';
import { CmxSidebarComponent, ICustomOption, ICustomSubOption } from '@cemex/cmx-sidebar-v1/dist';
import { ILegalEntity } from '@cemex-core/types-v2/dist/index.interface';
import { Broadcaster } from '@cemex-core/events-v1/dist';
import { SessionService,TranslationService } from '@cemex-core/angular-services-v2/dist';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Column, Table } from '@cemex/cmx-table-v1/dist';
import { CmxDialogV3 } from '@cemex/cmx-dialog-v3/dist';
import { AlertService } from '@cemex/cmx-alert-v2/dist';

import { ApplicationService } from './../../services/translationAPI.service'
import { ApplicationDTO } from '../../types/applicationDTO';
import { LanguageDTO } from '../../types/languateDTO';


@Component({
    templateUrl: './setup.view.html',
    styleUrls: ["./setup.view.scss"]
})
export class SetupComponent {
    form: FormGroup;
    formLang: FormGroup;
    private applicationsTable = new Table();
    private applicationsTableData: any[] = [];

    private languagesTable = new Table();
    private languagesTableData: any[] = [];
    private modalInAppCreate: boolean = true;

    @ViewChild(CmxDialogV3)
    private appDialog: CmxDialogV3;

    @ViewChild(CmxDialogV3)
    private langDialog: CmxDialogV3;


    constructor(private sessionService: SessionService,
        private eventBroadcaster: Broadcaster,

        private fb: FormBuilder,
        private fbLang: FormBuilder,

        private http: HttpClient,
        public a: AlertService,
        public api: ApplicationService,
        public t:TranslationService
    ) {
        this.populateTables();
        this.setApplicationTableColumns();
        this.setLanguageTableColumns();

    }

    public setApplicationTableColumns() {
        let applicationTableColumns: Column[] = [
            new Column('ID', 'applicationItemId', true, true, 'applicationItemId', 'applicationItemId'),
            new Column('Application name', 'applicationItemName', true, true, 'applicationItemName', 'applicationItemName'),
            new Column('Application description', 'applicationItemDescription', true, true, 'applicationItemDescription', 'applicationItemDescription'),
            new Column('Author', 'en_usk', true, true, 'en_usk', 'en_usk'),
            new Column('Technical Name', 'applicationItemTechnicalName', true, true, 'applicationItemTechnicalName', 'applicationItemTechnicalName'),
            new Column('Application', 'appk', true, true, 'appk', 'appk')
        ];
        this.applicationsTable.setColumns(applicationTableColumns);
    }
    public setLanguageTableColumns() {
        let languageTableColumns: Column[] = [
            new Column('ID', 'languageId', true, true, 'languageId', 'languageId'),
            new Column('Language name', 'languageName', true, true, 'languageName', 'languageName'),
            new Column('Language ISO', 'languageISO', true, true, 'languageISO', 'languageISO'),
            new Column('Country', 'languageCountry', true, true, 'languageCountry', 'languageCountry'),
            
            new Column('Author', 'en_usk1', true, true, 'en_usk1', 'en_usk1')

        ];
        this.languagesTable.setColumns(languageTableColumns);
        this.setFilters();
    }


    ngOnInit() {
        this.form = this.fb.group({
            appNameInput: ['', Validators.required],
            appDescriptionInput: ['', Validators.required],
            applicationItemTechnicalName:['',Validators.required]
        });
        this.formLang = this.fb.group({
            languageNameInput: ['', Validators.required],
            languageISOInput: ['', Validators.required],
            languageCountryInput: ['', Validators.required],
            countryCodeInput: ['', Validators.required],
            dayNamesInput: ['', Validators.required],
            monthNamesInput: ['', Validators.required],
            currencySymbol: ['', Validators.required],
            currencyFormat: ['', Validators.required],
            currencySymbolFloat: ['', Validators.required],
            currencyName: ['', Validators.required],
            formatDate: ['', Validators.required],
            formatTime: ['', Validators.required],
            decimalSeparator: ['', Validators.required],
            decimalNumbers: ['', Validators.required],
            thousandSeparator: ['', Validators.required],
            textFloat: ['', Validators.required],
            languageId:['']
        });
    }

    private populateTables(): void {
        this.api.getApps().subscribe(apps => {
            this.applicationsTableData = [];
            for (let i = 0; i < apps.length; i++) {
                const aux = {
                    $index: i,
                    applicationItemName: apps[i].applicationItemName,
                    applicationItemDescription: apps[i].applicationItemDescription,
                    applicationItemId: apps[i].applicationItemId,
                    applicationItemTechnicalName:apps[i].applicationItemTechnicalName
                };
                this.applicationsTableData.push(aux);
            }
            //this.setApplicationTableColumns();
        }, error => {

        })

        this.api.getLanguages().subscribe(apps => {
            this.languagesTableData = [];
            for (let i = 0; i < apps.length; i++) {
                const aux = Object.assign({
                    $index: i
                }, apps[i]);
                this.languagesTableData.push(aux);
            }
        }, error => {

        })


    }

    private setFilters(): void {
        this.applicationsTableData.forEach((data) => {
            this.applicationsTable.getColumn('applicationItemName').addFilter(data);
            this.applicationsTable.getColumn('applicationItemDescription').addFilter(data);
            this.applicationsTable.getColumn('applicationItemId').addFilter(data);
        });
    }

    private clickDropdownItem(): void {
        alert('Option clicked');
    }

    private saveNewApplication() {
        this.api.addApplication(new ApplicationDTO(this.form.value.appNameInput, 
            this.form.value.appDescriptionInput,
            this.form.value.applicationItemTechnicalName)).subscribe(appList => {
            this.form.reset({
                'appDescriptionInput': '',
                'appNameInput': ''
            })
            this.populateTables();
            this.a.openSuccess('App details saved correctly', 'Created as: ' + appList[appList.length - 1].applicationItemName, 0);
        },
            error => {
                this.a.openError(error._body, "Try a different value", 0);
            });
    }
    private saveNewLanguage() {
        this.api.addLanguage(
            new LanguageDTO(
                this.formLang.value.languageCountryInput,
                this.formLang.value.countryCodeInput,
                this.formLang.value.dayNamesInput,
                this.formLang.value.monthNamesInput,
                this.formLang.value.languageISOInput,
                this.formLang.value.languageNameInput,
                this.formLang.value.currencySymbol,
                this.formLang.value.currencySymbolFloat,
                this.formLang.value.currencyFormat,
                this.formLang.value.currencyName,
                this.formLang.value.formatDate,
                this.formLang.value.formatTime,
                this.formLang.value.decimalSeparator,
                this.formLang.value.decimalNumbers,
                this.formLang.value.thousandSeparator,
                this.formLang.value.textFloat,
                this.formLang.value.languageId
            )).subscribe(appList => {
                this.formLang.reset({
                    'languageCountryInput': '',
                    'countryCodeInput': '',
                    'dayNamesInput': '',
                    'monthNamesInput': '',
                    'languageNameInput': '',
                    'languageISOInput': '',
                    'currencySymbol': '',
                    'currencyFormat': '',
                    //'currencySymbolFloat':'',
                    'currencyName': '',
                    'formatDate': '',
                    'formatTime': '',
                    'decimalSeparator': '',
                    'decimalNumbers': '',
                    'thousandSeparator': ''
                    //'textFloat': ''
                })
                this.populateTables();
                this.a.openSuccess('App details saved correctly', 'Created as: ' + appList[appList.length - 1].languageName + " (" + appList[appList.length - 1].languageISO + ")", 0);
            },
                error => {
                    
                    this.a.openError(error._body, "Try a different value", 0);
                });
    }


    private editLanguage(value: LanguageDTO) {
        this.modalInAppCreate = false;
        this.appDialog.open();
        this.formLang.reset({
            'languageCountryInput': value.languageCountry,
            'countryCodeInput': value.countryCode,
            'dayNamesInput': value.dayNames,
            'monthNamesInput': value.monthNames,
            'languageNameInput': value.languageName,
            'languageISOInput': value.languageISO,
            'currencySymbol': value.currencySymbol,
            'currencyFormat': value.currencyFormat,
            'currencySymbolFloat': value.currencySymbolFloat,
            'currencyName': value.currencyName,
            'formatDate': value.formatDate,
            'formatTime': value.formatTime,
            'decimalSeparator': value.decimalSeparator,
            'decimalNumbers': value.decimalNumbers,
            'thousandSeparator': value.thousandSeparator,
            'textFloat': value.textFloat,
            'languageId':value.languageId
        })
    }
    private openApplicationDialog() {
        this.modalInAppCreate = true;
        this.appDialog.open();
    }
    private openLanguageDialog() {
        this.modalInAppCreate = false;
        this.formLang.reset({
            'languageCountryInput': ''
        })
        this.appDialog.open();
    }

    private enableTechnicalMode(value){
        this.api.putTechnicalMode(value).subscribe(success=>{
            alert("Technical mode enabled in selected application for 10 seconds, the technical mode will deactivate by itself.");
        })
    }


}
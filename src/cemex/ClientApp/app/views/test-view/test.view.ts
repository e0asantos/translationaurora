import { Component, ViewChild, transition } from '@angular/core';
import { CmxSidebarComponent, ICustomOption, ICustomSubOption } from '@cemex/cmx-sidebar-v1/dist';
import { ILegalEntity } from '@cemex-core/types-v2/dist/index.interface';
import { Broadcaster } from '@cemex-core/events-v1/dist';
import { SessionService, TranslationService } from '@cemex-core/angular-services-v2/dist';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Column, Table } from '@cemex/cmx-table-v1/dist';
import { CmxDialogV3 } from '@cemex/cmx-dialog-v3/dist';
import { LanguageDTO } from '../../types/languateDTO';
import { ApplicationService } from '../../services/translationAPI.service';
import { ApplicationDTO } from '../../types/applicationDTO';
import { CmxDropdownComponent } from '@cemex/cmx-dropdown-v1/dist/components/cmx-dropdown';
import { TranslationDTO } from '../../types/translationDTO';
import { AlertService } from '@cemex/cmx-alert-v2/dist';
import { error } from 'util';
import { KeyPairTranslation, MassUploadDTO } from '../../types/massUploadDTO';
import { CmxTableComponent } from '@cemex/cmx-table-v1/dist';
import { Router, ActivatedRoute } from '@angular/router';
import { forEach } from '@angular/router/src/utils/collection';


@Component({
    templateUrl: './test.view.html',
    styleUrls: ["./test.view.scss"]
})
export class TestComponent {
    form: FormGroup;
    formInitial: FormGroup;
    private exampleTable = new Table();
    private exampleData: any[] = [];

    @ViewChild(CmxDialogV3)
    private dialog: CmxDialogV3;

    @ViewChild(CmxTableComponent)
    private translationsTable: CmxTableComponent;

    @ViewChild(CmxDropdownComponent)
    private languageDropDownComponent: CmxDropdownComponent;

    @ViewChild(CmxDropdownComponent)
    private appDropDownComponent: CmxDropdownComponent;

    private selectedLangugageString: string = "Select language"
    private selectedAppString: string = "Select application";

    private languagesAvailable: LanguageDTO[];
    private appsAvailable: ApplicationDTO[];

    private createMode: number = 0;
    private jsonFileStep: number = 0;
    private selectedTranslationToEdit: any;

    private selectedJSONFile: KeyPairTranslation[];
    private lastExtensionUploadFile:string;

    private translationFilterValue:string;

    private feeling:string ="NEUTRAL";

    constructor(private sessionService: SessionService,
        private eventBroadcaster: Broadcaster,
        private fb: FormBuilder,
        private http: HttpClient,
        private api: ApplicationService,
        private alert: AlertService,
        private router: Router,
        private t: TranslationService
    ) {

        const columns: Column[] = [
            new Column('ID', 'translationValueId', true, true, 'translationValueId', 'translationValueId'),
            new Column('Translation key', 'proxyTranslationKey', true, true, 'proxyTranslationKey', 'proxyTranslationKey'),
            new Column('Translation', 'translation', true, true, 'translation', 'translation'),
            new Column('Language', 'proxyLanguageName', true, true, 'proxyLanguageName', 'proxyLanguageName'),
            new Column('Language ISO', 'proxyLanguageISO', true, true, 'proxyLanguageISO', 'proxyLanguageISO'),
            new Column('Country', 'proxyLanguageCountry', true, true, 'proxyLanguageCountry', 'proxyLanguageCountry'),
            new Column('Application', 'proxyApplicationName', true, true, 'proxyApplicationName', 'proxyApplicationName')

        ];
        this.exampleTable.setColumns(columns);
        this.setFilters();
        this.getGeneralAPIInformation();
    }

    public getGeneralAPIInformation() {
        this.api.getLanguages().subscribe(languages => {
            this.languagesAvailable = languages;
        })
        this.api.getApps().subscribe(apps => {
            this.appsAvailable = apps;
        })
    }

    public redirectToView() {
        this.router.navigate(['/app/setup']);
    }

    public textChanging() {
        console.log(this.form.value.translationInput);
        this.api.getSentiment(this.form.value.translationInput).subscribe(feeling => {
            console.log(feeling);
            this.feeling = feeling.sentiment;
        });
    }

    ngOnInit() {
        this.form = this.fb.group({
            translationKeyInput: ['', Validators.required],
            applicationIdInput: ['', Validators.required],
            languageISOInput: ['', Validators.required],
            translationInput: ['', Validators.required]
        });

        this.formInitial = this.fb.group({
            applicationIdInput: ['', Validators.required],
            languageISOInput: ['', Validators.required],
            translationFilterInput:['']
        });

        this.formInitial.get('translationFilterInput').valueChanges.forEach(
            (value) => {
                this.translationFilterValue=value;
                if(this.formInitial.value["applicationIdInput"]!=null && this.formInitial.value["languageISOInput"]!=null && this.formInitial.value["applicationIdInput"]!="" && this.formInitial.value["languageISOInput"]!=""){
                    this.populateFilteredTranslations(this.formInitial.value["applicationIdInput"],this.formInitial.value["languageISOInput"]);
                }
            } )
    }

    private populateTranslations(): void {
        
        this.api.getTranslations().subscribe(translations => {
            this.exampleData = [];
            for (let i = 0; i < translations.length; i++) {
                let isInside = false;
                for (let q = 0; q < this.exampleData.length; q++) {
                    if (this.exampleData[q].translationValueId === translations[i].translationValueId) {
                        isInside = true;
                        this.exampleData[q].translation = translations[i].translation;
                    }
                }

                const aux = {
                    $index: i,
                    translationValueId: translations[i].translationValueId,
                    proxyTranslationKey: translations[i].translationKeyReferenced.key,
                    translation: translations[i].translation,
                    proxyLanguageName: translations[i].languageISO.languageName,
                    proxyLanguageISO: translations[i].languageISO.languageISO,
                    proxyLanguageCountry: translations[i].languageISO.languageCountry,
                    proxyApplicationName: translations[i].applicationId.applicationItemName
                };
                if (!isInside) {
                    this.exampleData.push(aux);
                }

            }
            this.setFilters();
        })
    }


    

    private populateFilteredTranslations(appId:number,langId:number): void {
        
        this.api.getTranslationsByFilter(appId,langId).subscribe(translations => {
            this.exampleData = [];
            for (let i = 0; i < translations.length; i++) {
                let isInside = false;
                for (let q = 0; q < this.exampleData.length; q++) {
                    if (this.exampleData[q].translationValueId === translations[i].translationValueId) {
                        isInside = true;
                        this.exampleData[q].translation = translations[i].translation;
                    }
                }
                

                const aux = {
                    $index: i,
                    translationValueId: translations[i].translationValueId,
                    proxyTranslationKey: translations[i].translationKeyReferenced.key,
                    translation: translations[i].translation,
                    proxyLanguageName: translations[i].languageISO.languageName,
                    proxyLanguageISO: translations[i].languageISO.languageISO,
                    proxyLanguageCountry: translations[i].languageISO.languageCountry,
                    proxyApplicationName: translations[i].applicationId.applicationItemName
                };
                if(aux.translation==null){
                    continue;
                }
                if (!isInside && (this.translationFilterValue===null || this.translationFilterValue==="" || aux.proxyTranslationKey.indexOf(this.translationFilterValue)!=-1 || aux.translation.indexOf(this.translationFilterValue)!=-1 ) ) {
                    this.exampleData.push(aux);
                } 

            }
            this.setFilters();
        })
    }

    public downloadFile() {
        let filteredItems = this.translationsTable.filteredItems();
        let jsonToExport: any = new Object();
        for (let i = 0; i < filteredItems.length; i++) {
            jsonToExport[filteredItems[i].proxyTranslationKey] = filteredItems[i].translation;
        }
        var a = document.createElement("a");
        var file = new Blob([JSON.stringify(jsonToExport)], { type: "application/json" });
        a.href = URL.createObjectURL(file);
        a.download = this.makeid()+".json";
        a.click();
    }
    public downloadFileAsCSV() {
        let filteredItems = this.translationsTable.filteredItems();
        let jsonToExport: string = "Label technical name | Label translated\n";
        
        for (let i = 0; i < filteredItems.length; i++) {
            jsonToExport+=filteredItems[i].proxyTranslationKey+"|"+filteredItems[i].translation+"\n";
        }
        var a = document.createElement("a");
        var file = new Blob([jsonToExport], { type: "text/x-csv" });
        //var file = new Blob([jsonToExport], { type: "application/json" });
        a.href = URL.createObjectURL(file);
        //a.href = window.URL.createObjectURL(file.getBlob('text/plain'));
        a.download = this.makeid()+'.csv';
        a.textContent = jsonToExport;
        a.click();
        alert("The values are separated by pipes |");
    }

    public editTranslation(item: any) {
        this.selectedTranslationToEdit = item;
        this.form.reset({
            'translationKeyInput': item.proxyTranslationKey,
            'translationInput': item.translation,
            'languageISOInput': item.proxyLanguageISO,
            'applicationIdInput': item.proxyApplicationName
        })
        this.createMode = 1;
        this.dialog.open();
        console.log(item);
    }
    public makeid() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
      
        for (var i = 0; i < 5; i++)
          text += possible.charAt(Math.floor(Math.random() * possible.length));
      
        return text;
    }

    public getLanguageAndApp() {
        //console.log(this.formInitial.value);
        
        /*
            SEND THE DATA TO NEW API
            
        */
        this.populateFilteredTranslations(this.formInitial.value["applicationIdInput"],this.formInitial.value["languageISOInput"]);
    }

    public modifyTranslation() {
        let modifiedTranslation: TranslationDTO = new TranslationDTO();
        modifiedTranslation.translationValueId = this.selectedTranslationToEdit.translationValueId;
        modifiedTranslation.translation = this.form.value.translationInput;
        this.api.putTranslation(modifiedTranslation).subscribe(succes => {
            this.alert.openSuccess('Translation saved correctly', 'You can close this window ', 0);
            //this.populateTranslations(); //deprecated
            this.populateFilteredTranslations(this.formInitial.value["applicationIdInput"],this.formInitial.value["languageISOInput"]);
        })
    }

    private setFilters(): void {
        this.exampleData.forEach((data) => {
            this.exampleTable.getColumn('proxyLanguageName').addFilter(data);
            this.exampleTable.getColumn('proxyLanguageISO').addFilter(data);
            this.exampleTable.getColumn('proxyLanguageCountry').addFilter(data);
            this.exampleTable.getColumn('proxyApplicationName').addFilter(data);
        });
    }

    private clickLangDropdownItem(lang: LanguageDTO): void {
        let currentValue = this.form.value;
        currentValue.languageISOInput = lang.languageId;
        this.form.setValue(currentValue);
        this.selectedLangugageString = lang.languageName + " (" + lang.languageISO + ")";
    }
    private clickAppDropdownItem(app: ApplicationDTO): void {
        let currentValue = this.form.value;
        currentValue.applicationIdInput = app.applicationItemId;
        this.form.setValue(currentValue);
        this.selectedAppString = app.applicationItemName;
    }
    private clickLang(lang: LanguageDTO): void {
        let currentValue = this.formInitial.value;
        currentValue.languageISOInput = lang.languageId;
        this.formInitial.setValue(currentValue);
        this.selectedLangugageString = lang.languageName + " (" + lang.languageISO + ")";
    }



    private clickApp(app: ApplicationDTO): void {
        let currentValue = this.formInitial.value;
        currentValue.applicationIdInput = app.applicationItemId;
        this.formInitial.setValue(currentValue);
        this.selectedAppString = app.applicationItemName;
    }

    private saveNewTranslation() {
        this.api.addTranslation(new TranslationDTO(this.form.value.translationKeyInput, this.form.value.translationInput,
            this.form.value.languageISOInput, this.form.value.applicationIdInput)).subscribe(createdTranslation => {
                this.alert.openSuccess('Translation saved correctly', 'Created translation ' , 0);
                this.form.reset({
                    'translationKeyInput': '',
                    'translationInput': ''
                })
                //this.populateTranslations(); //deprecated method call that brings all the translations
                this.populateFilteredTranslations(this.formInitial.value["applicationIdInput"],this.formInitial.value["languageISOInput"]);
            }, error => {
                this.alert.openError("Error",error._body, 0);
            })
    }

    private massUploadDialog() {
        this.jsonFileStep=0;
        this.createMode = 2;
        this.form.reset({
            'translationKeyInput': '',
            'translationInput': '',
            'languageISOInput': this.formInitial.value["languageISOInput"],
            'applicationIdInput': this.formInitial.value["applicationIdInput"],
        })
        this.dialog.open();
    }
    private massUpload() {
        let mass: MassUploadDTO = new MassUploadDTO();
        mass.applicationId = this.form.value.applicationIdInput;
        mass.languageId = this.form.value.languageISOInput;
        mass.massJson = this.selectedJSONFile;
        this.api.addMassTranslation(mass).subscribe(success => {
            this.alert.openSuccess('Translation saved correctly', 'You can close this window ', 0);
            //this.populateTranslations();
            
        }, error => {

        })
    }

    private openDialog() {
        this.createMode = 0;
        this.form.reset({
            'translationKeyInput': '',
            'translationInput': '',
            'languageISOInput': this.formInitial.value["languageISOInput"],
            'applicationIdInput': this.formInitial.value["applicationIdInput"],
        })
        this.dialog.open();
        console.log(this.form.value);
    }

    private doSomethingOnClose(): void {
        //do some action
    }

    private doSomethingOnOpen(): void {
        //do some action
    }

    private clickUpload() {
        this.jsonFileStep=0;
        let file = document.getElementById('hiddenFileField');
        file.click();
        var reader = new FileReader();
        reader.onloadend = (e) => {
            var data = reader.result;
            let nativeObject;

            try {
                nativeObject = JSON.parse(data);
            } catch (error) {
                if(this.lastExtensionUploadFile.indexOf(".csv")!=-1){
                    let tmpCSV=e.currentTarget["result"].split("\n");
                    tmpCSV.shift();
                    nativeObject=new Object();
                    tmpCSV.forEach(element => {
                        let splitedKeyPair=element.split("|");
                        nativeObject[splitedKeyPair[0]]=splitedKeyPair[1];
                    });
                } else {
                    this.jsonFileStep=0;
                    return;
                }
                
            }

            this.selectedJSONFile = [];
            Object.keys(nativeObject).forEach((key) => {
                let pair: KeyPairTranslation = new KeyPairTranslation();
                pair.key = key;
                pair.value = nativeObject[key];
                
                this.selectedJSONFile.push(pair)
            });
            this.form.reset({
                'translationKeyInput': 'ok',
                'translationInput': 'ok'
            })
            this.jsonFileStep=2;
        }
        this.jsonFileStep=1;
        setTimeout(() => {
            try {
                if(file["files"].length>0){
                    
                    this.lastExtensionUploadFile=file["files"][0].name;
                    reader.readAsText(file["files"][0]);
                } else {
                    this.jsonFileStep=0;
                }
            } catch (error) {
                console.warn("File failed to load " + error);
                this.jsonFileStep=0;
                return;
            }
        }, 5000)

    }

    private closeDialog(): void {
        //do some action
        this.dialog.close();
    }
}
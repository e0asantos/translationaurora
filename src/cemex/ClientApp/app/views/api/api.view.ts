import { Component, ViewChild, transition } from '@angular/core';
// import { CmxSidebarComponent, ICustomOption, ICustomSubOption } from '@cemex/cmx-sidebar-v1/dist';
import { ILegalEntity } from '@cemex-core/types-v2/dist/index.interface';
import { Broadcaster } from '@cemex-core/events-v1/dist';
import { SessionService, TranslationService } from '@cemex-core/angular-services-v2/dist';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Column, Table } from '@cemex/cmx-table-v1/dist';
import { CmxDialogV3 } from '@cemex/cmx-dialog-v3/dist';
import { LanguageDTO } from '../../types/languateDTO';
import { ApplicationService } from '../../services/translationAPI.service';
import { ApiManagerService } from '../../services/ApiManager.service';
import { ApplicationDTO } from '../../types/applicationDTO';
import { CmxDropdownComponent } from '@cemex/cmx-dropdown-v1/dist/components/cmx-dropdown';
// import { TranslationDTO } from '../../types/translationDTO';
import { AlertService } from '@cemex/cmx-alert-v2/dist';
import { error } from 'util';
// import { KeyPairTranslation, MassUploadDTO } from '../../types/massUploadDTO';
import { CmxTableComponent } from '@cemex/cmx-table-v1/dist';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiEnvironmentDTO, ApiURLDTO, ApiRecordDTO, ApiApplicationItemDTO } from '../../types/apiDTO';


@Component({
    templateUrl: './api.view.html',
    styleUrls: ["./api.view.scss"]
})
export class ApiComponent {
    form: FormGroup;
    private exampleTable = new Table();
    private exampleData: any[] = [];
    private apiRecords: any[] = [];

    @ViewChild(CmxDialogV3)
    private dialog: CmxDialogV3;

    @ViewChild(CmxTableComponent)
    private apiRecordsTable: CmxTableComponent;

    @ViewChild(CmxDropdownComponent)
    private envDropDownComponent: CmxDropdownComponent;

    @ViewChild(CmxDropdownComponent)
    private appDropDownComponent: CmxDropdownComponent;
    
    @ViewChild(CmxDropdownComponent)
    private urlDropDownComponent: CmxDropdownComponent;

    private selectedEnvString: string = "Select environment"
    private selectedAppString: string = "Select application";
    private selectedUrlString: string = "Select URL";

    // private languagesAvailable: LanguageDTO[];
    private envsAvailable: ApiEnvironmentDTO[];
    private urlsAvailable: ApiURLDTO[];
    private appsAvailable: ApplicationDTO[];

    private createMode: number = 0;
    private jsonFileStep: number = 0;
    private selectedTranslationToEdit: any;
    private selectedApiToEdit: any;

    // private selectedJSONFile: KeyPairTranslation[];
    constructor(private sessionService: SessionService,
        private eventBroadcaster: Broadcaster,
        private fb: FormBuilder,
        private http: HttpClient,
        private app: ApplicationService,
        private api: ApiManagerService,
        private alert: AlertService,
        private router: Router,
        private t: TranslationService
    ) {

        this.populateApis();
        const columns: Column[] = [
            new Column('ID', 'apiRecordId', true, true, 'apiRecordId', 'apiRecordId'),
            new Column('Api Key', 'apiKey', true, true, 'proxy', 'proxyLanguageISO'),
            new Column('Api Name', 'name', true, true, 'name', 'name'),
            new Column('Api URL', 'proxyURL', true, true, 'proxyURL', 'proxyURL'),
            new Column('Api Env', 'proxyEnvironment', true, true, 'proxyEnvironment', 'proxyEnvironment'),
            new Column('App Container', 'appContainer', true, true, 'appContainer', 'appContainer'),
            new Column('Date Created', 'dateCreated', true, true, 'dateCreated', 'dateCreated'),
            new Column('Application', 'proxyApplicationName', true, true, 'proxyApplicationName', 'proxyApplicationName'),
            new Column('Created By', 'authorEmail', true, true, 'authorEmail', 'authorEmail')

        ];
        this.exampleTable.setColumns(columns);
        this.setFilters();
        this.getGeneralAPIInformation();
    }

    public getGeneralAPIInformation() {
        this.api.getEnvironments().subscribe(envs => {
            this.envsAvailable = envs;
        })
        this.api.getURLs().subscribe(urls => {
            this.urlsAvailable = urls;
        })
        this.app.getApps().subscribe(apps => {
            this.appsAvailable = apps;
        })
    }

    public redirectToView() {
        this.router.navigate(['/app/api']);
    }

    ngOnInit() {
        this.form = this.fb.group({
            nameInput: ['', Validators.required],
            applicationIdInput: ['', Validators.required],
            appContainerInput: ['', Validators.required],
            urlInput: ['', Validators.required],
            envInput: ['', Validators.required]
        });
    }

    private populateApis(): void {
        this.api.getApiRecords().subscribe(apiRecords => {
            this.apiRecords = apiRecords;
            if (this.exampleData.length == 0) {
                this.exampleData = [];
            }

            for (let i = 0; i < apiRecords.length; i++) {
                let isInside = false;
                for (let q = 0; q < this.exampleData.length; q++) {
                    if (this.exampleData[q].apiRecordId === apiRecords[i].apiRecordId) {
                        isInside = true;
                        this.exampleData[q].name = apiRecords[i].name;
                    }
                }

                let thedate = apiRecords[i].dateCreated.valueOf() * 1000

                const aux = {
                    $index: i,
                    apiKey: apiRecords[i].apiKey,
                    name: apiRecords[i].name,
                    appContainer: apiRecords[i].appContainer,
                    dateCreated: new Date(thedate).toDateString(),
                    apiRecordId: apiRecords[i].apiRecordId,
                    proxyEnvironment: apiRecords[i].apiEnv.environment,
                    proxyURL: apiRecords[i].apiURL.url,
                    proxyApplicationName: apiRecords[i].appId.applicationItemName,
                    authorEmail: apiRecords[i].authorEmail,
                };

                if (!isInside) {
                    this.exampleData.push(aux);
                }

            }
            this.setFilters();
        })
    }

    public downloadFile() {
        let filteredItems = this.apiRecordsTable.filteredItems();
        let jsonToExport: any = new Object();
        for (let i = 0; i < filteredItems.length; i++) {
            jsonToExport[filteredItems[i].name] = filteredItems[i].name;
        }
        var a = document.createElement("a");
        var file = new Blob([JSON.stringify(jsonToExport)], { type: "application/json" });
        a.href = URL.createObjectURL(file);
        a.download = name;
        a.click();
    }

    public openDisableModal(item: any) {
        this.selectedApiToEdit = item;
        this.form.reset({
            'nameInput': item.name,
            'envInput': item.proxyEnvironment,
            'urlInput': item.proxyURL,
            'appContainerInput': item.appContainer,
            'applicationIdInput': item.proxyApplicationName
        })
        this.createMode = 2;
        this.dialog.open();
    }

    public disableApiRecord() {
        let selectedRecord = this.apiRecords[this.selectedApiToEdit.$index];

        let modifiedApi: ApiRecordDTO = new ApiRecordDTO(
            selectedRecord.apiRecordId,                     // apiRecordId
            this.selectedApiToEdit.proxyApplicationName,    // name
            selectedRecord.appContainer,                    // appContainer
            selectedRecord.apiEnv,                          // apiEnv
            selectedRecord.apiURL,                          // apiURL
            selectedRecord.dateCreated,                     // dateCreated
            selectedRecord.apiKey,                          // apiKey
            selectedRecord.authorEmail,                     // authorEmail
            new ApiApplicationItemDTO(selectedRecord.appId.applicationItemId, selectedRecord.appId.applicationItemId),                          // appId
            true                                            // disabled
        );

        this.api.putApiRecord(modifiedApi).subscribe(succes => {
            this.alert.openSuccess('Api record saved correctly', 'You can close this window ', 0);
            this.populateApis();
        });
    }

    public editApi(item: any) {
        this.selectedApiToEdit = item;
        this.form.reset({
            'nameInput': item.name,
            'envInput': item.proxyEnvironment,
            'urlInput': item.proxyURL,
            'appContainerInput': item.appContainer,
            'applicationIdInput': item.proxyApplicationName
        });

        // Get the values for dropDown placeholders
        this.selectedEnvString = item.proxyEnvironment;
        this.selectedAppString = item.proxyApplicationName;
        this.selectedUrlString = item.proxyURL;

        // Get non-edited Id values for envInput, urlInput, applicationIdInput,
        // just in case the user doesn't change some of the drop-down options.
        let selectedRecord = this.apiRecords[this.selectedApiToEdit.$index];
        let currentValue = this.form.value;
        currentValue.envInput = selectedRecord.apiEnv.apiEnvironmentId;
        currentValue.urlInput = selectedRecord.apiURL.apiURLId;
        currentValue.applicationIdInput = selectedRecord.appId.applicationItemId;
        this.form.setValue(currentValue);

        // Open the Modal
        this.createMode = 1;
        this.dialog.open();
    }

    public modifyApi() {
        let selectedRecord = this.apiRecords[this.selectedApiToEdit.$index];

        let modifiedApi: ApiRecordDTO = new ApiRecordDTO(
            this.selectedApiToEdit.apiRecordId,                 // apiRecordId
            this.selectedApiToEdit.proxyApplicationName,        // name
            this.form.value.appContainerInput,                  // appContainer
            new ApiEnvironmentDTO(this.form.value.envInput),    // apiEnv*
            new ApiURLDTO(this.form.value.urlInput),            // apiURL*
            this.selectedApiToEdit.dateCreated,                 // dateCreated
            this.selectedApiToEdit.apiKey,                      // apiKey
            this.selectedApiToEdit.authorEmail,                 // authorEmail
            new ApiApplicationItemDTO(this.form.value.applicationIdInput, this.form.value.applicationIdInput.toString()), // appId*
            selectedRecord.disabled                             // disabled
        );

        this.api.putApiRecord(modifiedApi).subscribe(succes => {
            this.alert.openSuccess('Api record saved correctly', 'You can close this window ', 0);
            this.populateApis();
        })
    }

    private setFilters(): void {
        this.exampleData.forEach((data) => {
            this.exampleTable.getColumn('proxyEnvironment').addFilter(data);
            this.exampleTable.getColumn('proxyURL').addFilter(data);
            this.exampleTable.getColumn('proxyApplicationName').addFilter(data);
        });
    }

    private clickEnvDropdownItem(env: ApiEnvironmentDTO): void {
        let currentValue = this.form.value;
        currentValue.envInput = env.apiEnvironmentId;
        this.form.setValue(currentValue);
        this.selectedEnvString = env.environment;
        console.log("env dropdown: ", this.form.value.envInput)
    }

    private clickURLDropdownItem(url: ApiURLDTO): void {
        let currentValue = this.form.value;
        currentValue.urlInput = url.apiURLId;
        this.form.setValue(currentValue);
        this.selectedUrlString = url.url;
        console.log("url dropdown: ", this.form.value.urlInput)
    }

    private clickAppDropdownItem(app: ApplicationDTO): void {
        let currentValue = this.form.value;
        currentValue.applicationIdInput = app.applicationItemId;
        this.form.setValue(currentValue);
        this.selectedAppString = app.applicationItemName;
        console.log("app dropdown: ", this.form.value.applicationIdInput)
    }

    private openDialog() {
        this.createMode = 0;
        this.form.reset({
            'appContainerInput': '',
            'nameInput': '',
            'envInput': '',
            'urlInput': '',
            'applicationIdInput': ''
        })
        this.dialog.open();
    }

    private saveNewApiRecord() {
        let apikkey = btoa(this.form.value.nameInput + this.form.value.appContainerInput)
        let datetoday = parseInt(((new Date().valueOf()) / 1000).toFixed(0))
        let user = sessionStorage.getItem("username")
        var newRecord = new ApiRecordDTO(
            undefined,                                      // apiRecordId
            this.form.value.nameInput,                      // name
            this.form.value.appContainerInput,              // appContainer
            new ApiEnvironmentDTO(this.form.value.envInput),// apiEnv
            new ApiURLDTO(this.form.value.urlInput),        // apiURL
            datetoday,                                      // dateCreated
            apikkey,                                        // apiKey
            user,                                           // authorEmail
            new ApiApplicationItemDTO(null, this.form.value.applicationIdInput),  // appId
            false                                           // disabled
        )
        // console.log(btoa());//encode
        // console.log(atob());//unencode
        

        this.api.addApiRecord(newRecord).subscribe(createdApiRecord => {
            this.form.reset({
                'nameInput': '',
                'appContainerInput': '',
                'applicationIdInput': '',
                'urlInput': '',
                'envInput': ''
            })
            this.populateApis();
            this.alert.openSuccess('Api record saved correctly', '', 0);
        }, error => {
            this.alert.openError(error, error, 0);
        })
    }

    private closeDialog(): void {
        //do some action
        this.dialog.close();
    }
}
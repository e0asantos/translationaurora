import { CmxCheckboxModule } from '@cemex/cmx-checkbox-v1/dist';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CORE_COMMON_PROVIDERS } from '@cemex-core/angular-services-v2/dist';
import { CommonModule } from '@angular/common';
import { ApiComponent } from './api.view';
import { Routes, RouterModule } from '@angular/router';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CmxButtonModule } from '@cemex/cmx-button-v1/dist';


export const ROUTES: Routes = [
    { path: '', component: ApiComponent },
];

@NgModule({
    imports: [
        RouterModule.forChild(ROUTES),
        CmxButtonModule,
        CommonModule,
        FlexLayoutModule,
        CmxCheckboxModule
    ],
    declarations: [ApiComponent],
    exports: [ApiComponent,CmxButtonModule],
    providers: [
        CORE_COMMON_PROVIDERS,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class ApiModule { }
